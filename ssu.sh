#!/usr/bin/env zsh

ssh-keygen
# .. now go update github, gitlab
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
curl -sfL git.io/antibody | sh -s - -b /usr/local/bin
git clone git@gitlab.com:thedatabase/dotfiles.git .dotfiles
cd .dotfiles
./install
brew install go direnv
brew tap homebrew/cask-fonts && brew cask install font-source-code-pro
brew tap railwaycat/emacsmacport
brew install emacs-mac
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python get-pip.py
cd .dotfiles
git submodule update --init --recursive .
brew install kind
antibody bundle < ~/.dotfiles/zsh_plugins.txt > ~/.zsh_plugins.sh

#!/bin/zsh

# Antibody plugins
source ~/.zsh_plugins.sh

# Zfunctions (for completion etc)
fpath=( ~/.zfunc "${fpath[@]}" )

# homebrew completion
if type brew &>/dev/null; then
    FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH
fi

# run compinit (all completion etc paths to be added before here)
autoload -Uz compinit
compinit

#options
setopt hist_ignore_dups
setopt append_history
setopt share_history
setopt inc_append_history
setopt extended_glob
setopt auto_cd # Auto change to a dir without typing cd

#env
export HISTSIZE=10000
export HISTFILESIZE=100000
export SAVEHIST=10000
export HISTFILE=~/.zsh_history
export HISTDUP=erase
export LEIN_JVM_OPTS="-XX:TieredStopAtLevel=1"
PATH=$PATH:$HOME/opt/bin
PATH=$PATH:/Library/TeX/texbin
PATH=$PATH:$HOME/.emacs.d/bin
export PATH=$HOME/.local/bin:$PATH
export EDITOR=emacsclient

#aliases
alias e='emacsclient -n'
alias em='emacsclient'
alias ep='em ~/.zshrc'

#functions
updatezsh() {
 antibody bundle <~/.dotfiles/zsh_plugins.txt >~/.zsh_plugins.sh
 antibody update
}

# direnv
eval "$(direnv hook zsh)"
[ -f "${GHCUP_INSTALL_BASE_PREFIX:=$HOME}/.ghcup/env" ] && source "${GHCUP_INSTALL_BASE_PREFIX:=$HOME}/.ghcup/env"
export PATH="/usr/local/sbin:$PATH"

# gcloud
source "/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.zsh.inc"
source "/usr/local/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.zsh.inc"

